{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}

module Main where

import Data.Binary
import Data.Binary.Get
import Data.ByteString as BS
import Data.ByteString.Lazy as BL
import Data.ByteString.Char8 as BC8
import Data.Word
import GHC.Generics
import Network.Connection
import System.Exit
import System.Process.ByteString

data PackagePrelude = PackagePrelude
	{ garbage :: Word32
	, len :: Word32
	} deriving Generic

instance Binary PackagePrelude where
	get = PackagePrelude <$> getWord32le <*> getWord32le
	put = undefined

getBoostPackage :: Connection -> IO BS.ByteString
getBoostPackage conn = do
	prelude <- connectionGetExact conn 8
	let pp = decode $ BL.fromStrict prelude
	connectionGetExact conn $ fromIntegral $ len pp

main :: IO ()
main = do
	ctx <- initConnectionContext
	let host = "freeorion-lt.dedyn.io"
	conn <- connectTo ctx $ ConnectionParams host 12346 Nothing Nothing
	-- TODO implement as PackagePrelude's put
	let hello8 = BS.pack
		[ 5, 0, 0, 0
		, 0x87, 1, 0, 0]
	(ExitSuccess, version, _) <- readProcessWithExitCode "xmlstarlet" ["sel", "-t", "-v", "//version/string", "/home/l29ah/.config/freeorion/config.xml"] ""
	(ExitSuccess, cookie, _) <- readProcessWithExitCode "xmlstarlet" ["sel", "-t", "-v", "//cookie", "/home/l29ah/.config/freeorion/config.xml"] ""
	let helloxml = BS.concat ["<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n<!DOCTYPE boost_serialization>\n<boost_serialization signature=\"serialization::archive\" version=\"17\">\n<player_name>L29Ah</player_name>\n<client_type>1</client_type>\n<client_version_string>", version, "</client_version_string>\n<cookie>", cookie, "</cookie>\n</boost_serialization>\n\n"]
	let hello = BS.concat [hello8, helloxml]
	connectionPut conn hello
	-- skip garbage packets
	getBoostPackage conn
	getBoostPackage conn
	getBoostPackage conn	-- checksums
	getBoostPackage conn	-- -1
	getBoostPackage conn	-- chats

	dat <- getBoostPackage conn
	BC8.putStrLn dat
